/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.a70iii.mavenproject1;

import java.util.Scanner;

/**
 *
 * @author Surap
 */
public class Func_OX {

    static String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
    static String currentPlayer = "O";
    static int column;
    static int row;
    static int countTurn = 0;

    public static void main(String[] args) {
        while (true) {
            playGame();
            continueGame();
        }
    }

    private static void continueGame() {
        Scanner kb = new Scanner(System.in);
        String conState;
        System.out.print("Continue (Y/N) ? : ");
        conState = kb.next();
        table = new String[][] { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
        countTurn = 0;
        if (conState.equals("N")) {
            System.out.println("Thank You For Playing the Game.");
            System.out.println("Goodbye.");
            System.exit(0);
        }
    }

    private static void playGame() {
        printWelcome();
        while (true) {
            countTurn++;
            printTable();
            printTurn();
            inputNumber();
            if (isWin()) {
                printTable();
                printWin();
                break;
            }
            if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            switchPlayer();
        }
    }

    private static void printWin() {
        System.out.println(currentPlayer + " Win!!!");
    }

    private static void printDraw() {
        System.out.println("Draw!!!");
    }

    private static boolean isDraw() {
        if (countTurn == 9) {
            countTurn = 0;
            return true;
        }
        return false;
    }

    private static boolean isWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkDiag()) {
            return true;
        } else if (checkAntiDiag()) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (!table[row][i].equals(currentPlayer)) {
                return false;

            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (!table[i][column].equals(currentPlayer)) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiag() {
        for (int i = 0; i < 3; i++) {
            if (!table[i][i].equals(currentPlayer))
                return false;
        }
        return true;
    }

    private static boolean checkAntiDiag() {
        for (int i = 0; i < 3; i++) {
            if (!table[i][(3 - 1) - i].equals(currentPlayer))
                return false;
        }
        return true;
    }

    private static void switchPlayer() {
        if (currentPlayer.equals("O")) {
            currentPlayer = "X";
        } else {
            currentPlayer = "O";
        }
    }

    private static void inputNumber() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please Input Number to Place : ");
        int numberSelect = kb.nextInt();
        if (numberSelect > 9 || numberSelect < 1) {
            System.out.println("Invalid input! Try Again.");
            inputNumber();
            return;
        }
        column = (numberSelect - 1) % 3;
        row = (numberSelect - 1) / 3;
        while (table[row][column].equals("X") || table[row][column].equals("O")) {
            System.out.println("Try Again!!!");
            System.out.print("Please Input Number to Place : ");
            numberSelect = kb.nextInt();
            column = (numberSelect - 1) % 3;
            row = (numberSelect - 1) / 3;
        }
        table[row][column] = currentPlayer;
    }

    private static void printTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to OX Games");
        System.out.println("Table of numbers");
    }
}
